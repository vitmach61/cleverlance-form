import React from 'react';
import { 
    Label, 
    Checkbox
} from '@fluentui/react';
import { useId } from '@fluentui/react-hooks';

interface FormInputProps {
    label: string;
    id: string;
    value: boolean;
    setValue: (value: boolean) => void;
}

export const FormCheckBox = (props: FormInputProps) => {
    const inputId = useId(props.id);

    const onChange = (ev: React.FormEvent<HTMLElement | HTMLInputElement> | undefined, checked: boolean | undefined) => {
        if (checked){
            props.setValue(checked);
        } else {
            props.setValue(false);
        }
    };

    return (
        <div className="formInput">
            <Label htmlFor={inputId}>{props.label}</Label>
            <Checkbox onChange={onChange} id={inputId}/>
        </div>
    );
}