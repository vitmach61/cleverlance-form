import React from 'react';
import { 
    Label, 
    DatePicker, 
    DayOfWeek, 
    IDatePicker, 
    IDatePickerStrings, 
    mergeStyleSets
} from '@fluentui/react';
import { useId } from '@fluentui/react-hooks';


interface FormInputProps {
    label: string;
    id: string;
    value: Date | null | undefined;
    setValue: (value: Date | null | undefined) => void;
}

const DayPickerStrings: IDatePickerStrings = {
    months: [
      'Leden',
      'Únor',
      'Březen',
      'Duben',
      'Květen',
      'Červen',
      'Červenec',
      'Srpen',
      'Září',
      'Říjen',
      'Listopad',
      'Prosinec',
    ],
  
    shortMonths: ['Led', 'Úno', 'Bře', 'Dub', 'Kvě', 'Čvn', 'Čvc', 'Srp', 'Zář', 'Říj', 'Lis', 'Pro'],
  
    days: ['Neďele', 'Pondělí', 'Úterý', 'Středa', 'Čtvrtek', 'Pátek', 'Sobota'],
  
    shortDays: ['Ne', 'Po', 'Út', 'St', 'Čt', 'Pá', 'So'],
  
    goToToday: 'Dnes',
    prevMonthAriaLabel: 'Předešlý měsíc',
    nextMonthAriaLabel: 'Další měsíc',
    prevYearAriaLabel: 'Minulý rok',
    nextYearAriaLabel: 'Další rok',
    closeButtonAriaLabel: 'Zavřít',
    monthPickerHeaderAriaLabel: '{0}, Vyber pro změnu měsíce',
    yearPickerHeaderAriaLabel: '{0}, Vyber pro zmenu roku',
  
    isRequiredErrorMessage: 'Start date is required.',
  
    invalidInputErrorMessage: 'Invalid date format.',
};
  
const controlClass = mergeStyleSets({
control: {
    margin: '0 0 15px 0',
    maxWidth: '300px',
},
});

const onFormatDate = (date?: Date): string => {
return !date ? '' : date.getDate() + '. ' + (date.getMonth() + 1) + '. ' + (date.getFullYear());
};

const firstDayOfWeek = DayOfWeek.Monday;
const desc = 'This field is required. One of the support input formats is year dash month dash day.';

export const FormCalendar = (props: FormInputProps) => {
    const inputId = useId(props.id);

    const datePickerRef = React.useRef<IDatePicker>(null);


    const onParseDateFromString = (val: string): Date => {
        const date = props.value || new Date();
        const values = (val || '').trim().split('. ');
        console.log( values );
        const day = val.length > 0 ? Math.max(1, Math.min(31, parseInt(values[0], 10))) : date.getDate();
        const month = val.length > 1 ? Math.max(1, Math.min(12, parseInt(values[1], 10))) - 1 : date.getMonth();
        let year = val.length > 2 ? parseInt(values[2], 10) : date.getFullYear();
        if (year < 100) {
            year += date.getFullYear() - (date.getFullYear() );
        }
        return new Date(year, month, day);
    };

    return (
        <div className="formInput">
            <Label htmlFor={inputId}>{props.label}</Label>
            <DatePicker
                id={inputId}
                componentRef={datePickerRef}
                className={controlClass.control}
                isRequired={false}
                allowTextInput={true}
                ariaLabel={desc}
                firstDayOfWeek={firstDayOfWeek}
                strings={DayPickerStrings}
                value={props.value!}
                onSelectDate={props.setValue}
                formatDate={onFormatDate}
                parseDateFromString={onParseDateFromString}
                />
        </div>
    );
}