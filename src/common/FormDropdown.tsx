import React from 'react';
import { 
    Label, 
    Dropdown,
    IDropdownOption,
    IDropdownStyles
} from '@fluentui/react';
import { useId } from '@fluentui/react-hooks';

interface FormInputProps {
    label: string;
    id: string;
    value: IDropdownOption | null;
    setValue: (value: IDropdownOption | null) => void;
}

const dropdownStyles: Partial<IDropdownStyles> = {
    dropdown: { width: 300 },
};

  
  const options: IDropdownOption[] = [
    { key: 'Hlavní město Praha', text: 'Hlavní město Praha' },
    { key: 'Jihočeský kraj', text: 'Jihočeský kraj' },
    { key: 'Jihomoravský kraj', text: 'Jihomoravský kraj' },
    { key: 'Karlovarský kraj', text: 'Karlovarský kraj' },
    { key: 'Kraj Vysočina', text: 'Kraj Vysočina' },
    { key: 'Královéhradecký kraj', text: 'Královéhradecký kraj' },
    { key: 'Liberecký kraj', text: 'Liberecký kraj' },
    { key: 'Moravskoslezský kraj', text: 'Moravskoslezský kraj' },
    { key: 'Olomoucký kraj', text: 'Olomoucký kraj' },
    { key: 'Pardubický kraj', text: 'Pardubický kraj' },
    { key: 'Plzeňský kraj', text: 'Plzeňský kraj' },
    { key: 'Středočeský kraj', text: 'Středočeský kraj' },
    { key: 'Ústecký kraj', text: 'Ústecký kraj' },
    { key: 'Zlínský kraj', text: 'Zlínský kraj' },
  ];

export const FormDropdown = (props: FormInputProps) => {
    const inputId = useId(props.id);

    const onChange = (event: React.FormEvent<HTMLDivElement>, option?: IDropdownOption<IDropdownOption>): void => {
        if( option )
            props.setValue(option);
    };
    

    return (
        <div className="formInput">
            <Label htmlFor={inputId}>{props.label}</Label>
            <Dropdown
                options={options}
                styles={dropdownStyles}
                onChange={onChange}
            />
        </div>
    );
}