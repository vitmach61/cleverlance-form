import React from 'react';
import { 
    Label, 
    TextField, 
} from '@fluentui/react';
import { useId } from '@fluentui/react-hooks';

interface FormInputProps {
    label: string;
    id: string;
    value: string | null;
    setValue: ( value: string ) => void;
}


export const FormText = (props: FormInputProps) => {
    const inputId = useId(props.id);

    const onChangeInput = React.useCallback(
        (event: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>, newValue?: string) => {
          props.setValue(newValue || '');
        },
        [props],
    );

    return (
        <div className="formInput">
            <Label htmlFor={inputId}>{props.label}</Label>
            <TextField id={inputId} value={ props.value || "" } onChange={onChangeInput} />
        </div>
    );
}