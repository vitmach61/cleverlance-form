import React from 'react';
import { 
    DefaultButton, 
} from '@fluentui/react';
import { useId } from '@fluentui/react-hooks';

interface FormInputProps {
    label: string;
    id: string;
    onClick: () => void;
}


export const FormButton = (props: FormInputProps) => {
    const inputId = useId(props.id);

    return (
        <div className="formInput">
            <DefaultButton text={props.label} id={inputId} onClick={props.onClick} />
        </div>
    );
}