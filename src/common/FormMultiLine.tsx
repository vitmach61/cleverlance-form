import React from 'react';
import {  
    TextField, 
} from '@fluentui/react';
import { useId } from '@fluentui/react-hooks';

interface FormInputProps {
    id: string;
    text: string;
    value: string;
    setValue: (value: string) => void;
}


export const FormMultiline = (props: FormInputProps) => {
    const inputId = useId(props.id);

    const onChangeInput = React.useCallback(
        (event: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>, newValue?: string) => {
          props.setValue(newValue || '');
        },
        [props],
    );

    return (
        <div className="formInput">
            <TextField multiline rows={8} id={inputId} value={props.value} onChange={onChangeInput}/>
        </div>
    );
}