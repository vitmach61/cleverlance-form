import React from 'react';

import { MessageBar } from '@fluentui/react';

export const PopUp = (props:any) => {
    const reset = React.useCallback(() => {props.hide(false)}, [props]);

    return (
    <MessageBar
        messageBarType={props.type}
        isMultiline={false}
        onDismiss={reset}
        dismissButtonAriaLabel="Zavřít"
    >
        {props.text}
    </MessageBar>
    )
}