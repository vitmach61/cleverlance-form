import React, { useState } from 'react';
import {IDropdownOption, Stack, MessageBarType} from '@fluentui/react';

import { FormText, FormCalendar, FormCheckBox, FormDropdown, FormMultiline, FormButton, PopUp } from './common';

export const Wrapper = () => {
    const [nameInput, setNameInput] = useState<string | null>(null);
    const [surnameInput, setSurnameInput] = useState<string | null>(null);
    const [emailInput, setEmailInput] = useState<string | null>(null);
    const [calendarInput, setCalendarInput] = useState<Date | null | undefined>(null);
    const [regionInput, setRegionInput] = useState<IDropdownOption | null>(null);
    const [reactInput, setReactInput] = useState<boolean>(false);
    const [multilineInput, setmultilineIntput] = useState<string>("");
    const [saved, setSaved] = useState<boolean>(false);
    const [error, setError] = useState<boolean>(false);
    const [errorNotJSON, setErrorNotJSON] = useState<boolean>(false);

    const onSave = () => {
        const tryJSON = { name: nameInput, surname: surnameInput, email: emailInput, calendar: calendarInput, region: regionInput, react: reactInput }

        setmultilineIntput( JSON.stringify(tryJSON, (key, value) => {
            if ( value !== null && value !== undefined && value !== ""){
                return value;
            } 
        }, "\t") );

        setNameInput(null);
        setSurnameInput(null);
        setEmailInput(null);
        setCalendarInput(null);
        setRegionInput(null);
        setReactInput(false);
        setSaved(true);
    }

    const onLoad = () => {
        if (multilineInput === null || multilineInput === undefined || multilineInput === "") {
            setErrorNotJSON( true );
            return;
        } 

        try {
            JSON.parse(multilineInput);
        } catch (e) {
            setErrorNotJSON( true );
            return;
        }

        const index = new Map();
        index.set( "name", { type: "string", setState: setNameInput } );
        index.set( "surname", { type: "string", setState: setSurnameInput } );
        index.set( "email", { type: "string", setState: setEmailInput } );
        index.set( "calendar", { type: "string", setState: setCalendarInput } );
        index.set( "region", { type: "object", setState: setRegionInput } );
        index.set( "react", { type: "boolean", setState: setReactInput } );

        const content = JSON.parse(multilineInput); 

        for ( const x in content ) {
            if (!index.has(x) || typeof content[x] !== index.get(x).type) {
                console.log( x )
                setError(true);
                return;
            }
            if (x === "calendar") {
                const date = new Date();
                const values = (content[x] || '').trim().split('-');
                values[2] = values[2].substring(0, values[2].indexOf('T'));
                console.log(values, content[x]);
                const day = content[x].length > 0 ? Math.max(1, Math.min(31, parseInt(values[2], 10))) + 1 : date.getDate();
                const month = content[x].length > 1 ? Math.max(1, Math.min(12, parseInt(values[1], 10))) -1 : date.getMonth();
                let year = parseInt(values[0], 10);
            
                index.get(x).setState(new Date(year, month, day));
            } else 
                index.get(x).setState(content[x]);

        }

    }

    return (
        <div className="Wrapper">
            { (saved) && <PopUp text="Úspěšně uloženo" hide={setSaved}/> }
            { (error) && <PopUp text="Špatný formát JSONu" hide={setError} type={MessageBarType.error} /> }
            { (errorNotJSON) && <PopUp text="chyba v syntaxi JSONu / prázdný JSON" hide={setErrorNotJSON} type={MessageBarType.error} />}
            <h1>Cleverlance formulář</h1>
            <Stack 
                horizontal 
                tokens={{
                    childrenGap: 'l1',
                    padding: 'l1',
                }}
                style={{
                        alignItems: 'center',
                        display: 'flex',
                        justifyContent: 'center',
                    }} 
            >
                <FormText 
                    label={'Jméno'}
                    id={'name_input'}
                    value={nameInput}
                    setValue={setNameInput}
                />
                <FormText 
                    label={'Příjmení'}
                    id={'surrname_input'}
                    value={surnameInput}
                    setValue={setSurnameInput}
                />
            </Stack>
            <Stack 
                horizontal 
                tokens={{
                    childrenGap: 'l1',
                    padding: 'l1',
                }}
                style={{
                        alignItems: 'center',
                        display: 'flex',
                        justifyContent: 'center',
                    }} 
            >
                <FormText 
                    label={'Email'}
                    id={'email_input'}
                    value={emailInput}
                    setValue={setEmailInput}
                />
                <FormCheckBox
                    label={'React'}
                    id={'react_input'}
                    value={reactInput}
                    setValue={setReactInput}
                />
            </Stack>
            <Stack 
                horizontal 
                tokens={{
                    childrenGap: 'l1',
                    padding: 'l1',
                }}
                style={{
                        alignItems: 'center',
                        display: 'flex',
                        justifyContent: 'center',
                    }} 
            >
                <FormCalendar
                    label={'Datum Narození'}
                    id={'datNar_input'}
                    value={calendarInput}
                    setValue={setCalendarInput}
                />
                <FormDropdown
                    label={'Kraj'}
                    id={'region_input'}
                    value={regionInput}
                    setValue={setRegionInput}
                />
            </Stack>
            <Stack 
                horizontal 
                tokens={{
                    childrenGap: 'l1',
                    padding: 'l1',
                }}
                style={{
                        alignItems: 'center',
                        display: 'flex',
                        justifyContent: 'center',
                    }} 
            >
            <FormButton
                id={'save_button'}
                label={'Uložit'}
                onClick={onSave}
            />
            <FormButton
                id={'load_button'}
                label={'Načíst'}
                onClick={onLoad}
            />
            </Stack>

            <FormMultiline 
                id={'JSON_view'}
                text={"text"}
                value={multilineInput}
                setValue={setmultilineIntput}
            />
        </div>
    )
}