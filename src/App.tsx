import React, { useEffect, useState } from 'react';
import { initializeIcons } from '@fluentui/react/lib/Icons';

import { Wrapper } from "./Wrapper";



const init = async () => {
  initializeIcons();
  return true;
}

function App() {
  const [loaded, setLoaded] = useState(false);
  
    useEffect(() => {
      init()
        .then( status => { setLoaded(status) });
    }, []);


    
  if (loaded) {
    return (
      <Wrapper />
    );
  } else {
    return ( <div>loading</div> );
  }
}

export default App;
